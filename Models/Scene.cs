﻿using System.Collections.Generic;

namespace Yoroshiku.Engine.Models
{
    public class Scene
    {
        public Scene(string text) : this(text, new List<Transition>())
        {
        }

        public Scene(string text, List<Transition> transitions)
        {
            Text = text;
            Transitions = transitions;
        }

        public string Text { get; set; }

        public List<Transition> Transitions { get; set; }
    }
}
