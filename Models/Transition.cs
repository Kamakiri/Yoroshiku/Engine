﻿namespace Yoroshiku.Engine.Models
{
    public class Transition
    {
        public Transition(string text, Scene scene) : this(text, scene, new TrueCondition())
        {

        }

        public Transition(string text, Scene scene, Condition condition)
        {
            Text = text;
            Scene = scene;
            Condition = condition;
        }

        public string Text { get; set; }

        public Scene Scene { get; set; }

        public Condition Condition { get; set; }
    }
}
