﻿namespace Yoroshiku.Engine.Models
{
    public abstract class Condition
    {
        protected Condition(string description = "")
        {
            Description = description;
        }

        public string Description { get; set; }

        public abstract bool IsFullfiled();
    }
}