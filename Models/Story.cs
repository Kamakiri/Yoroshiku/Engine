﻿using Yoroshiku.Engine.Models.Abilities;

namespace Yoroshiku.Engine.Models
{
    public class Story
    {
        public Story(string title, string description, AbilitiesModel abilitiesModel, Scene firstScene)
        {
            Title = title;
            Description = description;
            AbilitiesModel = abilitiesModel;
            FirstScene = firstScene;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public Scene FirstScene { get; set; }

        public AbilitiesModel AbilitiesModel { get; set; }
    }
}
