﻿using System.Collections.Generic;
using System.Linq;

namespace Yoroshiku.Engine.Models
{
    public class EitherCondition : Condition
    {
        public List<Condition> Conditions { get; }

        public EitherCondition(List<Condition> conditions)
        {
            Conditions = conditions;
        }

        public override bool IsFullfiled()
        {
            return Conditions.Any(c => c.IsFullfiled());
        }
    }
}
