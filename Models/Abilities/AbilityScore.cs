﻿namespace Yoroshiku.Engine.Models.Abilities
{
    public class AbilityScore
    {
        public AbilityScore(Ability ability, int score)
        {
            Ability = ability;
            Score = score;
        }

        public Ability Ability { get; set; }

        public int Score { get; set; }
    }
}
