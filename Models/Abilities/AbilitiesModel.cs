﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Yoroshiku.Engine.Models.Abilities
{
    public class AbilitiesModel : IReadOnlyDictionary<string, Ability>
    {
        private readonly Dictionary<string, Ability> _abilities;

        public AbilitiesModel(string name, IEnumerable<Ability> abilities, ModifierTable modifierTable)
        {
            Name = name;
            ModifierTable = modifierTable;
            _abilities = abilities.ToDictionary(a => a.Name);
        }

        public int Count => _abilities.Count;

        public IEnumerable<string> Keys => _abilities.Keys;

        public ModifierTable ModifierTable { get; set; }

        public string Name { get; set; }

        public IEnumerable<Ability> Values => _abilities.Values;

        public Ability this[string key] => _abilities[key];

        public bool ContainsKey(string key) => _abilities.ContainsKey(key);

        public IEnumerator<KeyValuePair<string, Ability>> GetEnumerator() => _abilities.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public bool TryGetValue(string key, out Ability value) => _abilities.TryGetValue(key, out value);
    }
}
