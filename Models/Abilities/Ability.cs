﻿using Supernova.Foundation.Core;

namespace Yoroshiku.Engine.Models.Abilities
{
    public class Ability
    {
        public Ability(string name, string description = "")
        {
            Guard.NotNullOrWhitespace(name, nameof(name));
            Guard.NotNull(description, nameof(description));

            Name = name;
            Description = description;
        }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
