﻿using System;
using Supernova.Foundation.Core;

namespace Yoroshiku.Engine.Models.Abilities
{
    public class ModifierTable
    {
        private readonly Func<int, int> _calculator;

        public ModifierTable(Func<int, int> calculator)
        {
            Guard.NotNull(calculator, nameof(calculator));

            _calculator = calculator;
        }

        public int GetModifier(int score) => _calculator(score);

        public static ModifierTable Default { get; } = MakeStandard(10, 2);

        /// <summary>
        /// Makes a standard (D&amp;D-like) <see cref="ModifierTable"/>.
        /// </summary>
        /// <param name="zeroModifierScore">The score at which the modifier is zero.</param>
        /// <param name="scoreStep">The score step at which the modifier value increases by one.</param>
        /// <returns>The table.</returns>
        public static ModifierTable MakeStandard(int zeroModifierScore, int scoreStep)
        {
            Guard.StrictlyPositive(scoreStep, nameof(scoreStep));
            return new ModifierTable((int score) => (zeroModifierScore % 2 == 0
                ? score / scoreStep - zeroModifierScore / scoreStep
                : (score + 1) / scoreStep - (zeroModifierScore + 1) / scoreStep));
        }
    }
}