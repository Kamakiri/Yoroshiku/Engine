﻿namespace Yoroshiku.Engine.Models
{
    public class TrueCondition : Condition
    {
        public override bool IsFullfiled() => true;
    }
}