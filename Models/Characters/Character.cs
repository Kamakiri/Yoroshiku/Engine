﻿using System.Collections.Generic;
using Yoroshiku.Engine.Models.Abilities;

namespace Yoroshiku.Engine.Models.Characters
{
    public class Character
    {
        public Character(string givenName, string familyName, string biography, List<AbilityScore> abilityScores)
        {
            GivenName = givenName;
            FamilyName = familyName;
            Biography = biography;
            AbilityScores = abilityScores;
        }

        public string GivenName { get; set; }

        public string FamilyName { get; set; }

        public string Biography { get; set; }

        public List<AbilityScore> AbilityScores { get; set; }
    }
}
