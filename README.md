# Yoroshiku Engine

<img src="./icon.png" height="100px" />

The engine to run interactive fiction for Yoroshiku.

## License

![AGPLv3](agplv3.png)

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [LICENSE.txt](LICENSE.txt) for details.

## Credits

Project icon made by [Smashicons](https://www.flaticon.com/authors/smashicons "Smashicons") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon") is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")
